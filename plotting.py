import numpy as np
import matplotlib.offsetbox
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

################################################################################
def despine(ax):
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.set_xticks([])
    ax.set_yticks([])

################################################################################
def lightenPatches(ax):
    # Trick to set alpha for the patch only.
    # Credits: https://github.com/mwaskom/seaborn/issues/979
    for patch in ax.artists:
        rgba = patch.get_facecolor()
        if True:
            # Same color as if which is mixed with 60% transparency,
            # but without transparency.
            r, g, b, a = 0.6*np.array(rgba) + 0.4*np.array([1.,1.,1.,1.])
        else:
            # With 60% transparency.
            r, g, b, a = rgba
            a = 0.6
        patch.set_facecolor((r, g, b, a))

################################################################################
class AnchoredScaleBar(matplotlib.offsetbox.AnchoredOffsetbox):
    """
    size:   length of bar in data units
    pos:    Position of the scale bar (left end) in axes units.
            Note that arguments "loc", "bbox_to_anchor" and
            "bbox_transform" are overwritten if provided.
    extent: Height of bar ends in axes units


    https://stackoverflow.com/a/43343934/3388962
    """
    def __init__(self, size=1, pos=None, extent=0.03, label="", ax=None,
                 pad=0.4, borderpad=0., ppad=0, sep=2, prop=None,
                 frameon=True, linekw={}, **kwargs):
        if not ax:
            ax = plt.gca()
        trans = ax.get_xaxis_transform()
        sizeBar = matplotlib.offsetbox.AuxTransformBox(trans)
        line = Line2D([0,size],[0,0], **linekw)
        vline1 = Line2D([0,0],[-extent/2.,extent/2.], **linekw)
        vline2 = Line2D([size,size],[-extent/2.,extent/2.], **linekw)
        sizeBar.add_artist(line)
        sizeBar.add_artist(vline1)
        sizeBar.add_artist(vline2)
        txt = matplotlib.offsetbox.TextArea(label, minimumdescent=False)
        self.vpac = matplotlib.offsetbox.VPacker(children=[sizeBar,txt],
                                                 align="center",
                                                 pad=ppad,
                                                 sep=sep)
        if pos is not None:
            # Transform position coordinates to bbox coordinates [0,1]x[0,1]
            xlim = ax.get_xlim()
            ylim = ax.get_ylim()
            xext = xlim[1]-xlim[0]
            yext = ylim[1]-ylim[0]

            # TODO: also correct for paddings...
            diff = [0,0]
            trafo = lambda x,y: ((x-xlim[0])/xext-diff[0],
                                 (y-ylim[0])/yext+extent/2-diff[1])

            # Hacky. But self.set(bbox_transform=ax.transAxes) doesn't work..
            kwargs["loc"] = "upper left"
            kwargs["bbox_to_anchor"] = trafo(*pos)
            kwargs["bbox_transform"] = ax.transAxes
        matplotlib.offsetbox.AnchoredOffsetbox.__init__(self,
                                                        pad=pad,
                                                        borderpad=borderpad,
                                                        child=self.vpac,
                                                        prop=prop,
                                                        frameon=frameon,
                                                        **kwargs)

