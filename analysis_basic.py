################################################################################
# © by Norman Juchler and David Bächinger, 2019
################################################################################
import copy
import logging
import numpy as np
import pandas as pd
import seaborn as sns
from pathlib import Path as p
import matplotlib.pyplot as plt
import scipy.interpolate as interp

from plotting import AnchoredScaleBar, despine, lightenPatches
from utils import ensureDir, StructContainer

################################################################################
# SETTINGS
################################################################################
def loadConfigs():
    configs = StructContainer()
    configs.dataDir = p("../datasets/version2")
    configs.outDir = p("results")
    configs.metricsFile = configs.outDir / "metrics.csv"
    configs.scaleRef = 20 # mm
    configs.nPoints = 1000
    configs.saveLinePlots = True
    configs.saveAlignmentPlots = True
    configs.alignmentPlotStyle = "despined"     # despined or boxed
    configs.alignForMetrics = True
    configs.dpi = 100
    configs.alpha = 0.95    # Confidence level
    ensureDir(configs.outDir)
    return configs

################################################################################
# I/O
################################################################################
def setupLogging():
    logging.addLevelName(logging.WARNING, "WARN")
    datefmt = "%H:%M:%S"
    fmt = "%(asctime)s.%(msecs)03d - %(levelname)-5s: %(message)s"
    logging.basicConfig(format=fmt, datefmt=datefmt, level=logging.INFO)

################################################################################
def loadTestData():
    """
    Test data to validate some metrics.
    Circle:
        Radius R:           100
        Perimeter p:        2πR                         = 628.3185
        Curvature κ:        1/R                         = 0.01
        Total curvature L2: sqrt(∫κ**2*ds) = sqrt(2π/R) = 0.25066
    """
    logging.info("Creating test data...")
    n = 130
    t = np.linspace(0, 2*np.pi, n)
    x, y = np.cos(t), np.sin(t)
    x = (x+2)*100
    y = (y+2)*100
    return pd.DataFrame(np.column_stack([x, y]),
                        columns=["x", "y"])

################################################################################
def loadCases(configs):
    logging.info("Loading cases...")
    filePath = configs.dataDir / "cases.csv"
    cases = pd.read_csv(filePath)
    cases = rearrangeCasesTable(cases)
    cases.to_csv(configs.outDir/"cases.csv", index=False)
    logging.info("Loaded labels for %d cases from %d patients." %
                 (len(cases), len(cases["pat_no"].unique())))
    return cases

################################################################################
def rearrangeCasesTable(cases):
    """
    Transform the data frame.
    New columns:
        - pat_no
        - study_code
        - side
        - affected
        - endotype
        - hypoplasia
        - comments
    """
    dfL = cases.copy()
    dfR = cases.copy()
    dfL["side"] = "left"
    dfR["side"] = "right"
    df = pd.concat([dfL,dfR], axis=0)
    df["affected"] = ((df["side"]==df["affected_side"])
                      | (df["affected_side"]=="bilateral"))
    df = df.set_index(["pat_no", "study_code", "side"])
    df = df.sort_index()
    if True:
        logging.info("Note: The flag hypoplasia relies on the comments column.")
        df["hypoplasia"] = False
        rows = ((df["endotype"]=="hypoplastic") & (df["affected"]==True))
        df.loc[rows, "hypoplasia"] = True
        rows = ((df["endotype"]=="hypoplastic")
                & (df["comments"].str.startswith("BH")))
        df.loc[rows, "hypoplasia"] = True
    df = df.drop("affected_side", axis=1)
    # Move comments to back.
    col = df.pop("comments")
    df.insert(len(df.columns), col.name, col)
    df = df.reset_index()

    # Add a column "group" representing the five categories:
    #   deg+a:      Degenerative endotype, affected
    #   deg+na:     Degenerative endotype, not affected
    #   hypo+a:     Hypoplastic endotype, affected
    #   hypo+na+h:  Hypoplastic endotype, not affected, with hypoplasia
    #   hypo+na+nh: Hypoplastic endotype, not affected, without hypoplasia
    df["group"] = None
    df.loc[(df["endotype"]=="degenerative") &
           (df["affected"]==True), "group"] = "deg+a"
    df.loc[(df["endotype"]=="degenerative") &
           (df["affected"]==False), "group"] = "deg+na"
    df.loc[(df["endotype"]=="hypoplastic") &
           (df["affected"]==True), "group"] = "hypo+a"
    df.loc[(df["endotype"]=="hypoplastic") &
           (df["affected"]==False) &
           (df["hypoplasia"]==True), "group"] = "hypo+na+h"
    df.loc[(df["endotype"]=="hypoplastic") &
           (df["affected"]==False) &
           (df["hypoplasia"]==False), "group"] = "hypo-na-nh"
    return df

################################################################################
def getCasesSubGroups(cases):
    """
    Given the cases table, create the subgroups which will be compared to
    one another. Also provide labels and titles used for plots and file I/O.
    """
    groups = {}

    title = "All cases"
    label = "all"
    groups[label] = dict(title=title, label=label, cases=cases)

    title = "Left side"
    label = "left"
    casesLeft = cases[cases["side"]=="left"]
    groups[label] = dict(title=title, label=label, cases=casesLeft)

    title = "Right side"
    label = "right"
    casesRight = cases[cases["side"]=="right"]
    groups[label] = dict(title=title, label=label, cases=casesRight)

    title = "All cases of degenerative endotype"
    label = "deg"
    casesDeg = cases[(cases["endotype"]=="degenerative")]
    groups[label] = dict(title=title, label=label, cases=casesDeg)

    title = "All cases of hypoplastic endotype"
    label = "hypo"
    casesHypo = cases[(cases["endotype"]=="hypoplastic")]
    groups[label] = dict(title=title, label=label, cases=casesHypo)

    title = "Affected cases of degenerative endotype"
    label = "deg-aff"
    casesDegAff = cases[(cases["endotype"]=="degenerative") &
                        (cases["affected"]==True)]
    groups[label] = dict(title=title, label=label, cases=casesDegAff)
    title = "Non-affected cases of degenerative endotype"
    label = "deg-nonaff"
    casesDegNonAff = cases[(cases["endotype"]=="degenerative") &
                           (cases["affected"]==False)]
    groups[label] = dict(title=title, label=label, cases=casesDegNonAff)

    title = "Affected cases of hypoplastic endotype"
    label = "hypo-aff"
    casesHypoAff = cases[(cases["endotype"]=="hypoplastic") &
                         (cases["affected"]==True)]
    groups[label] = dict(title=title, label=label, cases=casesHypoAff)

    title = "Non-affected cases of hypoplastic endotype"
    label = "hypo-nonaff"
    casesHypoNonAff = cases[(cases["endotype"]=="hypoplastic") &
                            (cases["affected"]==False)]
    groups[label] = dict(title=title, label=label, cases=casesHypoNonAff)

    title = "Non-affected cases of hypoplastic endotype with hypoplasia"
    label = "hypo-nonaff-hypo"
    casesHypoNonAffHypo = cases[(cases["endotype"]=="hypoplastic") &
                                (cases["affected"]==False) &
                                (cases["hypoplasia"]==True)]
    groups[label] = dict(title=title, label=label, cases=casesHypoNonAffHypo)

    title = "Non-affected cases of hypoplastic endotype without hypoplasia"
    label = "hypo-nonaff-nonhypo"
    casesHypoNonAffNonHypo = cases[(cases["endotype"]=="hypoplastic") &
                                   (cases["affected"]==False) &
                                   (cases["hypoplasia"]==False)]
    groups[label] = dict(title=title, label=label, cases=casesHypoNonAffNonHypo)

    # Set index for all selections.
    for group in groups.values():
        group["cases"] = group["cases"].set_index(["study_code", "side"])

    return groups

################################################################################
def loadLine(dataset, side, configs):
    filePath = configs.dataDir / "csv" / (dataset+side+".csv")
    if not filePath.is_file():
        logging.warning("File %s is missing." % filePath.name)
        return None
    points = pd.read_csv(filePath, index_col=0)
    points.columns = ["x", "y"]
    # Coordinates in ImageJ (where the points were picked) have origin in the
    # upper left corner. Let's move the origin to the lower left.
    points["y"] = -points["y"]
    return points

################################################################################
def loadImage(dataset, side, configs):
    filePath = configs.dataDir / "images" / (dataset+side+".PNG")
    if filePath.is_file():
        return plt.imread(str(filePath))
    else:
        return None

################################################################################
def savePlot(filePath, formats=[".png"], dpi=300):
    for fmt in formats:
        outDir = filePath.parent / fmt.replace(".", "")
        outFile = outDir / (filePath.stem+fmt)
        if ensureDir(outDir):
            plt.savefig(outFile, dpi=dpi)

################################################################################
def saveMetrics(metrics, configs):
    if ensureDir(configs.metricsFile.parent):
        metrics.to_csv(configs.metricsFile)

################################################################################
# PREPROCESSING
################################################################################
def scaleData(points, configs):
    scale = 1.
    p0,p1 = points.values[0:2]
    # TODO; remove this conditional!
    scaleRef = configs.scaleRef
    if np.linalg.norm(p1-p0) > 3*scaleRef and True:
        # Drop the first point, it does not belong to the line.
        # Protocol:     |point0-point1| = 20mm
        #               point1...pointN : line
        points = points.drop(index=points.index[0:1])
        scale = scaleRef / np.linalg.norm(p1-p0)
        points *= scale
    else:
        logging.warning("First two points should encode scale.")
    return scale, points

################################################################################
def flipLeftToRight(points, side):
    points = points.copy()
    shiftX = 0
    if side in ("L", "left"):
        points.x = -points.x
        # Shift by a bit (for better plotting).
        # This shift will be removed by alignment.
        shiftX = points.x.min()
        points.x -= shiftX
    elif side in ("R", "right"):
        pass
    else:
        assert(False) # Invalid side
    return points, shiftX

################################################################################
def interpolatePolylineExact(points, nPoints=1000):
    """
    Interpolate 2D points using B-splines.
    The method preserves the support points accurately.
    """
    tck, u = interp.splprep(points.T, s=0)
    u = np.linspace(0.0, 1.0, nPoints)
    x, y = interp.splev(u, tck)
    Dx, Dy = interp.splev(u, tck, der=1)
    DDx, DDy = interp.splev(u, tck, der=2)
    df = pd.DataFrame(np.column_stack([x, y, Dx, Dy, DDx, DDy]),
                      columns=["x", "y", "dx", "dy", "ddx", "ddy"])
    return df

################################################################################
def interpolatePolylineSmooth(points, nPoints=1000, error=0.0001):
    """
    Interpolate 2D points using smoothing splines.

    The method can be used to smooth/filter the support data, but
    accordingly does not perfectly preserve the support.

    Use the error to adjust the amount of smoothing.
    """
    x, y = points.values.T
    u = np.linspace(0.0, 1.0, len(x))
    t = np.linspace(0.0, 1.0, nPoints)
    std = error * np.ones_like(x)
    fx = interp.UnivariateSpline(u, x, w=1/np.sqrt(std))
    fy = interp.UnivariateSpline(u, y, w=1/np.sqrt(std))
    df = pd.DataFrame(np.column_stack([fx(t), fy(t),
                                       fx.derivative(1)(t),
                                       fy.derivative(1)(t),
                                       fx.derivative(2)(t),
                                       fy.derivative(2)(t)]),
                      columns=["x", "y", "dx", "dy", "ddx", "ddy"])
    return df

################################################################################
def plotInterpolatedData(samples, curve, image, scale, side,
                         shiftX, configs, color="r"):
    samples = samples.copy()
    curve = curve.copy()
    if image is not None:
        # Undo the transforms to match image.
        if side in ("left", "L"):
            samples.x = -(samples.x+shiftX)
            curve.x = -(curve.x+shiftX)
        samples.y = -samples.y
        curve.y = -curve.y
    if scale:
        samples /= scale
        curve /= scale
    fig, ax = plt.subplots()
    if image is not None:
        im = ax.imshow(image)
        if side in ("left", "L"):
            ax.text(30,50, "Line flipped backed", color="white")
    ax.plot(samples.x, samples.y, "x", color=color)
    ax.plot(curve.x, curve.y, color=color)
    ax.axis("off")

################################################################################
def packData(points, curve, scale):
    """
    This documents what we bundle.
        points: Support points picked by the medical operator (David).
                Represented by a dataframe with columns "x" and "y".
        curve:  Interpolation curve that connects the points.
                Again represented by a dataframe. Columns: x,y,dx,dy,ddx,ddy
        scale:  A float representing the scale.
    """
    return (points, curve, scale)

################################################################################
def unpackData(data):
    points, curve, scale = data
    return points, curve, scale

################################################################################
def preprocessDataset(dataset, side, configs):
    sideMap = {"left": "L", "right":"R"}
    side = sideMap[side]
    image = loadImage(dataset, side, configs)
    points = loadLine(dataset, side, configs)
    if points is None:
        curve = points = scale = None
        return packData(points, curve, scale)
    scale, points = scaleData(points, configs)
    points, shiftX = flipLeftToRight(points, side)
    interpolatePolyline = interpolatePolylineExact
    #interpolatePolyline = interpolatePolylineSmooth
    curve = interpolatePolyline(points, nPoints=configs.nPoints)
    if configs.saveLinePlots:
        plt.figure(1)
        plt.clf()
        plotInterpolatedData(points, curve, image, scale,
                             side, shiftX, configs=configs)
        filename = f"result-{dataset}{side}.png"
        filePath = configs.outDir/"plots"/filename
        savePlot(filePath, dpi=configs.dpi)
        plt.close()
    return packData(points, curve, scale)

################################################################################
def preprocessDatasets(cases, configs):
    datasets = {}
    for _, row in cases.iterrows():
        dataset = str(row["study_code"])
        side = row["side"]
        index = (row["study_code"], row["side"])
        data = preprocessDataset(dataset, side, configs)
        datasets[index] = data
    return datasets

################################################################################
# MORPHOMETRICS
################################################################################
def computeMetrics(points):
    def _computeCurvature(points):
        """
        Curvature: κ = (x'y"-y'x")/(x'**2+y'**2)^(3/2)
        """
        d = points
        curvature = (d.dx*d.ddy-d.dy*d.ddx) / np.power(d.dx**2+d.dy**2, 3/2)
        if False:
            plotCurvature(points, curvature, pId=500)
        return curvature

    def _computeLength(values):
        diffs = values[1:]-values[:-1]
        return np.linalg.norm(diffs, axis=1).sum()

    def _computeStraightLineLength(values):
        p0 = values[0,:]
        p1 = values[-1,:]
        return np.linalg.norm(p1-p0)

    def _totalCurvature(points, curvs):
        diffs = points[["x","y"]].values
        diffs = diffs[1:]-diffs[:-1]
        diffs = np.linalg.norm(diffs,axis=1)
        curvs = (curvs[1:]+curvs[:-1])/2
        return np.sqrt(np.sum(np.abs(curvs)*diffs))

    metrics = {}
    curvs = _computeCurvature(points).values
    curvs = copy.deepcopy(curvs)
    #curvs[curvs<0] = 0
    length0 = _computeStraightLineLength(points[["x","y"]].values)
    metrics["length"] = _computeLength(points[["x","y"]].values)
    metrics["tortuosity"] = metrics["length"] / length0
    # Curvature.
    metrics["curvature.posRatio"] = sum(curvs>0.)/len(curvs)
    metrics["curvature.mean"] = np.mean(curvs)
    metrics["curvature.L2"] = _totalCurvature(points, curvs)
    metrics["curvature.L2N"] = metrics["curvature.L2"] / metrics["length"]

    # To elaborate...
    if False:
        from utilities.maths import empiricalEntropy
        metrics["curvature.std"] = np.std(curvs)
        metrics["curvature.max"] = np.max(curvs)
        metrics["curvature.min"] = np.min(curvs)
        metrics["curvature.H"] = empiricalEntropy(curvs, differential=False)
        metrics["curvature.absMean"] = np.mean(np.abs(curvs))

        # Normalized by length.
        metrics["curvature.stdN"] = metrics["curvature.std"] / metrics["length"]
        metrics["curvature.maxN"] = metrics["curvature.max"] / metrics["length"]
        metrics["curvature.minN"] = metrics["curvature.min"] / metrics["length"]
        metrics["curvature.HN"] = metrics["curvature.H"] / metrics["length"]

    return metrics

################################################################################
def plotCurvature(curve, curvature, pId):
    """
    Useful for validation of the code.
    """
    i = pId
    d = curve
    c = curvature[i]
    r = 1/c
    n = np.array([-d.dy[i], d.dx[i]])
    n /= np.linalg.norm(n)
    p0 = np.array([d.x[i], d.y[i]])
    p1 = p0+n*r
    t = np.linspace(0,2*np.pi, 1000)
    cx = r*np.cos(t)
    cy = r*np.sin(t)
    cx += p1[0]
    cy += p1[1]
    plt.figure()
    plt.plot(d.x, d.y, "k")
    plt.plot([p0[0],p1[0]], [p0[1],p1[1]], "o-", color="grey")
    plt.plot(cx,cy, "r")
    plt.axis("square")
    plt.show()
    exit()

################################################################################
def compareMetrics(metrics1, metrics2):
    return {k: abs(d1-d2) for k, d1, d2 in zip(metrics1.keys(),
                                               metrics1.values(),
                                               metrics2.values())}

################################################################################
def computeAllMetrics(datasets, configs):
    datasets = copy.deepcopy(datasets)
    if configs.alignForMetrics:
        # Optionally align the curves. What is the impact on the metrics?
        logging.info("Aligning data before computing morphometrics.")
        curvesAligned, _ = alignCurves(datasets, configs)
        for key, curveAligned in curvesAligned.items():
            points, curve, scale = unpackData(datasets[key])
            curve0 = copy.deepcopy(curve)
            # This modifies the container (datasets[key]).
            curve.loc[:,["x","y"]] = curveAligned
    else:
        logging.info("Not aligning data before computing morphometrics.")

    allMetrics = {}
    for index, data in datasets.items():
        points, curve, scale = unpackData(data)
        if curve is None:
            continue
        metrics = computeMetrics(curve)
        metrics["scale"] = scale
        allMetrics[index] = metrics
    allMetrics = pd.DataFrame(allMetrics).T
    allMetrics.columns.names = ["metric"]
    allMetrics.index.names = ["study_code", "side"]

    if False:
        # Test: mesh dependency
        curveRef = interpolatePolyline(points, nPoints=1000)
        metricsRef = computeMetrics(curveRef)
        print(compareMetrics(metrics, metrics2))
    return allMetrics

################################################################################
def analyzeMetrics(cases, metrics, configs):
    def _createBoxplot(data, xName, yName):
        sns.set(style="ticks",)
        plt.clf()
        ax = sns.boxplot(x=xName, y=yName,
                         palette=sns.color_palette("hls", 5),
                         dodge=False,
                         data=data)
        lightenPatches(ax)
        #sns.despine()
        fileName = f"{xName}-vs-{yName}.png"
        filePath = configs.outDir/"analysis"/fileName
        #plt.tight_layout()
        plt.title(yName)
        savePlot(filePath, formats=[".pdf", ".svg"])

    logging.info("Analyzing metrics...")
    cases = cases.set_index(["study_code", "side"])
    data = cases.join(metrics, how="left")
    saveMetrics(data, configs)

    # Prepare box-plot.
    data = data.sort_values("group")
    for metric in metrics:
        _createBoxplot(data, xName="group", yName=metric)
    exit()

################################################################################
# ALIGNMENT
################################################################################
def alignCurves(datasets, configs):
    """
    This applies Procrustes analysis.

    Method A: Procrustes analysis from scipy.
              Problem: reflection cannot be disabled.
    Method B: A port from MATLAB's procrustes() function.
    """
    useMethodA = False
    if useMethodA:
        from scipy.spatial import procrustes
    else:
        from procrustes import procrustes

    # Arbitrarily pick first dataset as reference. Use the same reference,
    # regardless of the cases sub-group selected. (Good for  plotting).
    indexRef = next(iter(datasets)) # Pick first key.
    _, reference, _ = unpackData(datasets[indexRef])
    assert("x" in reference.columns and "y" in reference.columns)
    reference = reference[["x", "y"]].values
    # Shift the reference along the x-axis such that the plots start at "0".
    reference -= reference.min(axis=0)
    curvesAligned = {}
    isFirst = True
    for index in datasets.keys():
        if index==indexRef:
            continue
        points, curve, scale = unpackData(datasets[index])
        if curve is None:
            # Curve might not be available.
            continue
        curve = curve[["x", "y"]].values
        if useMethodA:
            curveRef, curve, disparity = procrustes(reference, curve)
            curvesAligned.setdefault(indexRef, curveRef)
        else:
            # Reflection should not be allowed! All lines are already
            # flipped to be on the right side.
            disparity, curve, trafo = procrustes(reference, curve,
                                                 reflection=False)
            #print(trafo)
            curveRef = reference

        # Set ref just once.
        if isFirst:
            curvesAligned[indexRef] = curveRef
            isFirst = False
        curvesAligned[index] = curve

    return curvesAligned, curvesAligned[indexRef]

################################################################################
def computeMeanCurve(curvesAligned, label, configs):
    """
    Given the aligned curves, compute the mean curve, the standard deviation
    along the line, and estimate of the 95% confidence interval (CI) for this
    average curve.

    Returns:
        meanCurve:      Mean curve np.ndarray with shape (nPoints, 2)
        sigmaCurve:     Principal directions per curve point, scaled by the
                        corresponding standard deviation (again per point).
        ciCurve:        Principal directions per curve point, scaled by half
                        the CI. The CI finally is defined by meanCurve-ciCurve
                        (lower bound) and meanCurve+ciCurve (upper bound).
    """
    # Detail: Using reduce is pretty fast. The code has been validated.
    from functools import reduce
    # Mean curve.
    fun = lambda a,b: a+b
    nCurves = len(curvesAligned)
    meanCurve = reduce(fun, curvesAligned.values())/nCurves
    nPoints = meanCurve.shape[0]

    # Compute the 95%-confidence interval for the mean curve.
    # Combine the CI of x and y values as (disp_x)^2+(disp_y)^2, where
    # disp_w = se_w*t_x.
    alpha = configs.alpha
    import scipy.stats as st
    x = np.stack([v[:,0] for v in curvesAligned.values()], axis=1)
    y = np.stack([v[:,1] for v in curvesAligned.values()], axis=1)
    t = st.t.ppf((1+alpha) / 2., nCurves-1)
    stdX = np.std(x,axis=1,ddof=1)
    stdY = np.std(y,axis=1,ddof=1)
    semX = st.sem(x,axis=1)
    semY = st.sem(y,axis=1)
    disp = t*np.sqrt(semX**2+semY**2)
    sigmaCurve = np.c_[stdX,stdY]
    sigma = np.linalg.norm(sigmaCurve, axis=1, keepdims=True)
    diffCurve = sigmaCurve/sigma

    p = meanCurve.T
    dp = np.zeros_like(p)
    dp[:,1:-1] = p[:,2:]-p[:,:-2]
    dp[:,0]    = p[:,1]-p[:,0]
    dp[:,-1]   = p[:,-1]-p[:,-2]
    normals = np.c_[dp[1],-dp[0]]
    normals /= np.linalg.norm(normals,axis=1, keepdims=True)
    # Task: Estimate the principal axis of dispersion along the
    # curve. This direction will be used to indicate the spread
    # in the data (confidence interval, tolerance interval)
    # Using the normals is less intuitive, if not plain wrong.
    # While using the diff curve also is wrong in general, it gives
    # an okay impression with the data at hand because the curves
    # have a negative slope while the error vectors normally point
    # into the "positive" quadrant of the Cartesian coordinate system.
    # The best solution requires the computation of the principal
    # axis of the per-point data (for every point on the meanCurve,
    # there are nCurves points associated with that averaged point).
    # Indeed, there's only a small difference between diffCurve and
    # the principal axis as shown in the plot below.
    principals = np.zeros_like(normals)
    sigmaPrinc = np.zeros_like(sigma)
    for i in range(x.shape[0]):
        cov = np.cov(x[i,:], y[i,:], ddof=1)
        w,v = np.linalg.eig(cov)
        idx = np.argsort(w)[::-1]
        w,v = w[idx], v[:,idx]
        orient = np.dot(v[:,0], normals[i,:])
        principals[i,:] = v[:,0] * (-1 if orient<0 else 1)
        sigmaPrinc[i] = np.sqrt(w[0])

    if label and configs.outDir and True:
        plt.figure()
        plt.plot(meanCurve[:,0], meanCurve[:,1], "r")
        for i in range(x.shape[0]):
            if i%(nPoints//20)==0:
                pp0 = meanCurve[i,:]
                pp1 = meanCurve[i,:]+principals[i,:]*sigmaPrinc[i]
                pp2 = meanCurve[i,:]+diffCurve[i,:]*sigma[i]
                plt.scatter(x[i,:], y[i,:], color="k", alpha=0.1)
                plt.plot([pp0[0],pp1[0]], [pp0[1],pp1[1]], "b")
                plt.plot([pp0[0],pp2[0]], [pp0[1],pp2[1]], "g")
        plt.axis("square")
        plt.title(label)
        filePath = configs.outDir / "details" / ("errors-%s" % label)
        savePlot(filePath, formats=[".pdf"], dpi=configs.dpi)
    #diffCurve = normals
    diffCurve = principals
    sigmaCurve = sigma*diffCurve
    ciCurve = np.c_[disp]*diffCurve

    # Average sum of squared error.
    fun = lambda a,b: a+np.sum((b-meanCurve)**2,axis=1,keepdims=True)
    sse = reduce(fun, curvesAligned.values(), np.zeros_like(meanCurve))
    sseMean = np.mean(sse/nCurves)
    sseStd = np.std(sse/nCurves, ddof=1)
    logging.info("Average sum of squared error: %.3f±%.3f (%s)",
                 sseMean, sseStd, label)
    return meanCurve, sigmaCurve, ciCurve

################################################################################
def plotAlignment(curvesAligned, meanCurve, sigmaCurve, ciCurve,
                  label, title, configs):
    plt.figure("Aligned curves")
    plt.gcf().clf()
    for curve in curvesAligned.values():
        if curve is None:
            continue
        plt.plot(curve[:,0], curve[:,1], c=[0.3]*3, lw=1, alpha=0.2)
    meanPosSigma = meanCurve+2*sigmaCurve
    meanNegSigma = meanCurve-2*sigmaCurve
    ciPos = meanCurve+ciCurve
    ciNeg = meanCurve-ciCurve
    plt.plot(meanPosSigma[:,0], meanPosSigma[:,1], color="r", lw=1, ls=":", alpha=0.7)
    plt.plot(meanNegSigma[:,0], meanNegSigma[:,1], color="r", lw=1, ls=":", alpha=0.7)
    plt.fill_between(meanCurve[:,0], y1=ciPos[:,1], y2=ciNeg[:,1], color="r", alpha=0.3)
    plt.plot(meanCurve[:,0], meanCurve[:,1], c="r", lw=1)
    filePath = configs.outDir/"plots"/f"curvesAligned-{label}.svg"

    plt.axis("square")
    plt.xlim([-4,37])
    plt.ylim([-5,25])
    plt.yticks(range(0,25,5))

    if configs.alignmentPlotStyle == "boxed":
        plt.xlabel("x (mm)")
        plt.ylabel("y (mm)")
    elif configs.alignmentPlotStyle == "despined":
        scaleBar = AnchoredScaleBar(size=5,
                                    pos=(30,0),
                                    label="5 mm",
                                    frameon=False,
                                    pad=0.,
                                    sep=2.,
                                    linekw=dict(lw=1, color="black"))
        # Move to (0,0) - tuned value.
        #scaleBar.set_bbox_to_anchor((81.2,51))
        plt.gca().add_artist(scaleBar)
        #despine(plt.gca())
    else:
        assert(False)

    plt.title(title)
    savePlot(filePath, formats=[".pdf", ".svg"], dpi=configs.dpi)
    plt.close()

################################################################################
def analyzeAlignment(cases, datasets, configs):
    casesGroups = getCasesSubGroups(cases)
    logging.info("Analyzing alignment...")

    curvesAligned, curveRef = alignCurves(datasets, configs)
    for group in casesGroups.values():
        curves = {k:curvesAligned[k] for k in group["cases"].index
                  if k in curvesAligned} # Curve might not be available.
        meanCurve, sigmaCurve, ciCurve, = computeMeanCurve(curvesAligned=curves,
                                                           label=group["label"],
                                                           configs=configs)
        plotAlignment(curves, meanCurve, sigmaCurve, ciCurve,
                      group["label"], group["title"], configs)

    for key, curveAligned in curvesAligned.items():
        points, curve, scale = unpackData(datasets[key])
        if configs.saveAlignmentPlots:
            # Plot the curves before and after alignment.
            plt.figure("alignment")
            plt.clf()
            plt.plot(curve.x, curve.y, "b", label="Original")
            plt.plot(curveAligned[:,0], curveAligned[:,1], "r", label="Aligned")
            plt.plot(curveRef[:,0], curveRef[:,1], "k:", label="Reference")
            plt.title("ID: %d-%s" % key)
            plt.legend()
            filePath = configs.outDir / "details" / ("alignment-%d%s" % key)
            savePlot(filePath, formats=[".pdf"])

################################################################################
def run():
    """
    Variables:
        configs:    Configs container. See loadConfigs().
        cases:      Cases tables, reorganized table dataDir/cases.csv
        datasets:   A dict mapping keys (study_code, side) to a tuple
                    consisting of (points, curve, scale):
                    points: Pick points, as table with columns x,y.
                    curve:  Interpolated curve, represented as table with
                            columns x,y,dx,dy,ddx,ddy.
                    scale:  Scale determined by the first two pick-points.
    """
    setupLogging()
    configs = loadConfigs()
    cases = loadCases(configs)
    datasets = preprocessDatasets(cases, configs)
    analyzeAlignment(cases, datasets, configs)
    metrics = computeAllMetrics(datasets, configs)
    analyzeMetrics(cases, metrics, configs)


################################################################################
if __name__ == "__main__":
    run()
